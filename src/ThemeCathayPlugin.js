import { FlexPlugin } from 'flex-plugin';
import React from 'react';

import * as Wow from "./WowBrandConfiguration";


export default class ThemeCathayPlugin extends FlexPlugin {
  name = 'ThemeCathayPlugin';

  init(flex, manager) {
    manager.updateConfig({ ...Wow });

  }
}
