const brandColor1 = "#006564";
const brandColor2 = "#006564";
//const brandColor3 = "#ffda00";
const brandColor3 = "#006564";

const brandTextColor = "#fff";
const brandTextColorInverted = "#006564";

const brandMessageBubbleColors = bgColor => ({
  Bubble: {
    background: bgColor,
    color: brandTextColor
  },
  Avatar: {
    background: bgColor,
    color: brandTextColor
  },
  Header: {
    color: brandTextColor
  }
});

const branded = {
  LoginView: {
    Container: {
      backgroundColor: "#220C34",
      backgroundImage: `url("https://graceful-yoke-4557.twil.io/assets/Background02%404x.svg")`
    }
  },
  UserCard: {
    background: brandColor1
  },
  MainContainer: {
    backgroundColor: "#222",
    // background: `linear-gradient(to bottom, #232526, #414345)`
  },
  MainHeader: {
    Container: {
      // background: `linear-gradient(to top, #25861e, #1c6917)`,
      background: "#006564",
      color: brandTextColor
    }
  },
  AgentDesktopView: {
    Panel1: {
      // background: `repeating-linear-gradient(-45deg,transparent 4px, #333 5px, #333 6px, transparent 10px)`,
      // backgroundColor: "#222"
      background: "transparent"
    },
    Panel2: {
      background: "transparent"
    }
  },
  Supervisor: {
    Container: {
      background: "transparent"
    }
  },
  TaskDetailsPanel: {
    Container: {
      background: "transparent"
    }
  },
  NoTasksCanvas: {
    Container: {
      background: "transparent"
    }
  },
  CRMContainer: {
    Placeholder: {
      Container: {
        background: "transparent"
      }
    }
  },
  Chat: {
    MessageListItem: {
      FromOthers: brandMessageBubbleColors(brandColor1),
      FromMe: brandMessageBubbleColors(brandColor2)
    },
    MessageInput: {
      Button: {
        background: brandColor1,
        color: brandTextColor
      }
    },
    MessagingCanvas: {
      Container: {
        background: "transparent"
      }
    }
  },
  TaskCanvasHeader: {
    Container: {
      background: brandColor1
    }
  },
  Tabs: {
    Container: {
      background: "transparent"
    },
    LabelsContainer: {
      background: `linear-gradient(#112211, #125430)`, //custom
      color: brandTextColor,
      marginLeft: 0,
      marginRight: 0
    }
  },
  SideNav: {
    Button: {
      background: brandColor3,
      color: brandTextColor
    },
    ButtonSelected: {
      background: brandColor3,
      color: brandTextColor
    },
    Container: {
      background: brandColor3,
      color: brandTextColor
    },
    Icon: {
      color: brandTextColor
    },
    IconSelected: {
      color: brandTextColor
    }
  }
};

const strings = {
  WelcomeMessage: "Welcome to Cathay Pacific"
};

const colorTheme = {
  baseName: "DarkTheme",
  overrides: branded
};
const componentProps = {
  CRMContainer: {
    uriCallback: task => {
      return task
        ? "https://twilioapac-dev-ed.my.salesforce.com/_ui/search/ui/UnifiedSearchResults?str=" +
            task.attributes.name
        : null;
    }
  },
  AgentDesktopView: {
    // showPanel2: true
    showFeedback: true
  },
  MainHeader: {
    logoUrl: "https://www.cathaypacific.com/content/dam/cx/designs/responsive/images/cathaypacific-en-white.svg"
  }
};

export { colorTheme, componentProps, strings };
