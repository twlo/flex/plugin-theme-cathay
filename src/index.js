import * as FlexPlugin from 'flex-plugin';
import ThemeCathayPlugin from './ThemeCathayPlugin';

FlexPlugin.loadPlugin(ThemeCathayPlugin);
